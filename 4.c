#include <stdio.h>

int main()
{
    int n=0, fact=0;
    printf("Enter the number : ");
    scanf("%d", &n);

    printf("Factors are \n");

    for(int i=1; i<=n; i++){
        if(n%i==0){
           printf("%d, ", i);
        }
    }
    printf("\b\b \n");

    return 0;
}

