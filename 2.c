#include <stdio.h>

int main()
{
    int num=0, x=0;
    printf("Enter the number : ");
    scanf("%d", &num);

    for(int i=2; i<num; i++){
       if(num%i==0){
            x=1;
            break;
       }
    }

    if(x==1){
        printf("'%d' is not a prime number \n", num);
    }
    else{
        printf("'%d' is a prime number \n", num);
    }

    return 0;
}


