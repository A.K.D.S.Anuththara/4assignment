#include <stdio.h>

int main()
{
    int n=0, rev=0, temp=0;
    printf("Enter a number : ");
    scanf("%d", &n);

    while(n!=0){
        temp = n%10;
        n = n/10;
        rev = (rev*10) + temp;
    }

    printf("%d ", rev);

    return 0;
}

